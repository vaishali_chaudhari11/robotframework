*** Test Cases ***
test_case_1
    [Documentation]    How to write test case with *RIDE* and \ *Robot* test automation framework?
    ...
    ...    To install and run Robot framework IDE (RIDE) on windows, run following commands:
    ...
    ...    1. Install Python: \ https://www.python.org/downloads/
    ...
    ...    2. pip install robotframework
    ...
    ...    3. pip install robotframework-ride
    ...
    ...    4. python -m robotide.__init__
    ...
    Log    Welcome in robot framework
    Log    Inside robot framework
    Log    End of the test case 1
